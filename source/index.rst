.. UMOP (UMass OMOP API) documentation master file, created by
   sphinx-quickstart on Tue Jun 22 20:56:35 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UMOP (UMass OMOP API)'s documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

  modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
